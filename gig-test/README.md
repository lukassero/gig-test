# Hello there!

Thank you for pretty cool technical challenge. I did enjoy it. Hope you are going to enjoy the results too.

### Compile command
- `npm i && gulp sass:prod`
- in case of `gulpInst.start.apply(gulpInst, toRun);` error plz refer to https://stackoverflow.com/questions/38719999/cannot-read-property-apply-of-undefined-gulp

### HTML templates

- `index.html` - registration form
- `index-ui.html` - UI library
- `index-ui-theme.html` - themed UI library

### Notes
- as stated in the instructions I let myself skip all JavaScript related parts of the task and focused solely on field of my expertize which is HTML and CSS

- all versions (mobile/tablet/desktop) use custom dropdown trigger, I haven't implemented any fully working dropdown as it would require use of some JS plugin

- switching from native select to a custom dropdown on desktop would be a matter of initializing plugin method mentioned above on a certain breakpoint

- `.psd` files were pretty messy, I hope it was intentional, but I managed

- using `Montserrat` font as an alternative to `Proxima` causes some inconsistency issues with design as it is wider, e.g.: https://monosnap.com/file/XqinCFhJRfNoLqxCeqYqT4zr91eUd9

- due to font rendering issues width `Montserrat` I also skipped `Action` font for main headline on desktop and sticked to one font family only, you need to believe me that I do know how to use `@font-face` rule : P

### Key concepts
- CSS architecture is based on `ITCSS` methodology by Harry Roberts https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/

- first all components were created and as standalone entities and displayed in form of mini UI Kit library, this type of approach is called style guide driven development

- only once components are ready custom registartion form styles are added

- main colors are set as custom properties, thanks to that color theme can be changed with ease

- you can use tab for navigation 

- dropdowns and inputs can accept an element on both of their sides

### Tested on
- macOs - Chrome, Firefox, Safari
- windows 10 - Chrome, Firefox, Edge (no IE11 in da house, and no, I don't feel sorry about it ;P)

### Improvements
Once styleguide is established it would be cool to build Design System to rule them all :))

## Cheers!
