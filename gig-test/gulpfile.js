// required
var gulp  = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var pxtorem = require('gulp-pxtorem');
var livereload = require('gulp-livereload');

// px to rem
var pxtoremOptions = {
  rootValue: 16,
  propWhiteList: [],
  selectorBlackList: [],
  mediaQuery: true,
  replace: true
};

// Sass
var input = './sass/**/*.scss';
var output = '.';

var sassOptionsDev = {
  errLogToConsole: true,
  outputStyle: 'expanded'
};

var sassOptionsProd = {
  errLogToConsole: true,
  outputStyle: 'compressed'
};

gulp.task('sass', function () {
  return gulp
    .src(input)
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptionsDev).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(output))
    .pipe(livereload());
});

gulp.task('sass:prod', function () {
  return gulp
    .src(input)
    .pipe(sass(sassOptionsProd).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(pxtorem(pxtoremOptions))
    .pipe(gulp.dest(output));
});

gulp.task('sass:watch', function () {
  livereload.listen();
  gulp.watch(input, gulp.series('sass'));
});
